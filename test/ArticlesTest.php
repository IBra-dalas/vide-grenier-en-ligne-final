<?php


use App\Models\Articles;
use PHPUnit\Framework\TestCase;

class ArticlesTest extends TestCase
{

    public function testSave()
    {
        $data = [
            'name' => 'test insertion',
            'description' => 'test description insertion',
            'user_id' => 1
        ];
        $this->assertNotEmpty(Articles::save($data));
    }

    public function testGetSuggest()
    {
        $this->assertNotEmpty(Articles::getSuggest());
    }

    public function testGetAll()
    {
        $listArticle = Articles::getAll('');
        $this->assertNotEmpty($listArticle);
    }

    public function testAddOneView()
    {
        // if the method doesn't work it'll throw an error but if it work it'll throw nothing (=null)
        $this->assertNull(Articles::addOneView(1));
    }

    public function testGetByUser()
    {
        $listArticles = Articles::getByUser(1);
        $this->assertNotNull($listArticles);
    }

    public function testGetOne()
    {
        $article = Articles::getOne(1);
        $this->assertNotNull($article[0]);
    }
}
