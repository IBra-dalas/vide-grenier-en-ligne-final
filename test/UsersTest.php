<?php


use App\Models\User;
use PHPUnit\Framework\TestCase;

/* Test For User object */

class UsersTest extends TestCase
{
    public array $data = [
    'username' => 'Quentin',
    'email' => 'quentin@gmail.com',
    'password' => 'quent@in23',
    'salt' => '***quentin23'
    ];

    public function testCreateUser(){

        $this->assertNotEmpty(User::createUser($this->data));
    }
 
    public function testGetByLogin(){
      $this->assertNotNull(User::getByLogin($this->data['email']));
    }
}


?>